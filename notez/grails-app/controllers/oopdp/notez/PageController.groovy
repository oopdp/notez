package oopdp.notez

import grails.web.RequestParameter
import oopdp.exp.Point

class PageController {

	static layout = "app"
    
    public static validPages = ['charts', 'tables', 'forms', 'elements', 'grid', 'blank']
	
    def index(@RequestParameter('s') String message)
    {
    	def p = new Person()
    	p.setAge(10)
    	p.setName(message)
        

    	flash.message = "NAME: "+p.getName()+" Sasa AGE: "+p.getAge()+" "
    	render(view: 'types/blank')
    }

	def landing()
	{
		
	}

	
    def home(@RequestParameter('p') String page) 
    {
		if(page == null){
			
		}
		else {
			if(validPages.contains(page)){
				render(view: 'types/'+page)
			}
			else {
				flash.message = 'Nope!';
				redirect(action: 'home')
			}
		}
		Point p = new Point()
		println p.printAgain()
		p.setMe(1.2, 4.3)
		println p.printAgain()
		p.setMe(11.2, 42.32)
		println p.printAgain()

    }

    def test()
    {
    	render "SSSSS"
    }

}
