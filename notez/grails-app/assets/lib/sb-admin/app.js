// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
// JQ
//= require js/jquery
//
// BS
//= require js/bootstrap
// 
// MORRIS CHARTS JS
//= require js/plugins/morris/raphael.min
//= require js/plugins/morris/morris
//= require js/plugins/morris/morris-data
//
// FLOT CHARTS JS
//= require js/plugins/flot/jquery.flot
//= require js/plugins/flot/jquery.flot.tooltip.min
//= require js/plugins/flot/jquery.flot.resize
//= require js/plugins/flot/jquery.flot.pie
//= require js/plugins/flot/flot-data

//= require_self

if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });
    })(jQuery);
}

console.log('HERE!');