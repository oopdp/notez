package oopdp.notez

class Note {

	String type
	String content
	Date created
	Date modified
	String tags

	static mapping = {
   		content type: 'text'
	}

    static constraints = {
    }
}
