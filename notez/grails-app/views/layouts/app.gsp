<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><g:layoutTitle default="ForexFolks" /></title>
    <asset:stylesheet src="sb-admin/app"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <g:layoutHead />
</head>

<body>

    <div id="wrapper">
        <g:render template="/partials/test" />
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <g:render template="/partials/brand" />
            <!-- Top Menu Items -->
            <g:render template="/partials/top_menu" />
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <g:render template="/partials/nav_bar" />
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <g:render template="/partials/page_heading" />
                

                <g:render template="/partials/alert" />


                <g:layoutBody/>
                

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <asset:javascript src="sb-admin/app"/>

</body>

</html>
