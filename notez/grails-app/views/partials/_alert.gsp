<g:if test="${flash.message}">
	<div class="row">
		<div class="col-lg-12">
			<div class="alert alert-info alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="fa fa-info-circle"></i>  <strong>Like SB Admin?</strong> ${flash.message}
			</div>
		</div>
	</div>
</g:if>